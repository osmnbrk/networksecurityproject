from flask import Flask
from flask_restful import reqparse, abort, Api, Resource

from endpoints import login
from endpoints import getInbox
from endpoints import sendMail
from endpoints import register
from endpoints import ApiPublicKey
from endpoints import getClientPub
from endpoints import getAtc

app = Flask(__name__)
api = Api(app)

api.add_resource(ApiPublicKey.ApiPublicKey, '/publicKey')
api.add_resource(register.Register, '/register')
api.add_resource(login.Login, '/login')
api.add_resource(getInbox.GetInbox, '/inbox')
api.add_resource(sendMail.SendMail, '/send')
api.add_resource(getClientPub.GetClientPub, '/clientPub')
api.add_resource(getAtc.GetAtc, '/download')


if __name__ == '__main__':
    app.run(debug=True, host = "0.0.0.0", port = 9797)
