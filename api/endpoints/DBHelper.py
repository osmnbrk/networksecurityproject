import sqlite3 as sql
import datetime


class DBHelper:
    def __init__(self):
        self.connection = sql.connect(r'C:\Users\OsmnBrk\Desktop\devel\networksecurityproject\api\network_security.sqlite')
        self.cursor = self.connection.cursor()
        query = """
        CREATE TABLE IF NOT EXISTS user (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, surname TEXT NOT NULL, email TEXT NOT NULL, password TEXT NOT NULL)
        """
        self.cursor.execute(query)
        query = """
        CREATE TABLE IF NOT EXISTS keyRelation (id INTEGER PRIMARY KEY AUTOINCREMENT, userId INTEGER NOT NULL, publicKey TEXT NOT NULL, lifeTime TEXT NOT NULL)
        """
        self.cursor.execute(query)
        self.connection.commit()
    def database_checker(self):
        query = """
        CREATE TABLE IF NOT EXISTS user (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, surname TEXT NOT NULL, email TEXT NOT NULL, password TEXT NOT NULL)
        """
        self.cursor.execute(query)
        query = """
        CREATE TABLE IF NOT EXISTS keyRelation (id INTEGER PRIMARY KEY AUTOINCREMENT, userId INTEGER NOT NULL, publicKey TEXT NOT NULL, lifeTime TEXT NOT NULL)
        """
        self.cursor.execute(query)
        self.connection.commit()
    def new_user(self, name, surname, email, password):
        try:
            query = f"INSERT INTO user (name, surname, email, username, password) VALUES ('{name}', '{surname}', '{email}', '{password}')"
            self.cursor.execute(query)
            id = self.cursor.lastrowid
            self.connection.commit()
            return id
        except Exception as error:
            print(error)
            return False
    def update_user_name(self, id, name):
        try:
            query = f"UPDATE user SET name = '{name}' WHERE id = {id}"
            self.cursor.execute(query)
            self.connection.commit()
            return True
        except Exception as error:
            print(error)
            return False
    def update_user_surname(self, id, surname):
        try:
            query = f"UPDATE user SET surname = '{surname}' WHERE id = {id}"
            self.cursor.execute(query)
            self.connection.commit()
            return True
        except Exception as error:
            print(error)
            return False
    def update_user_email(self, id, email):
        try:
            query = f"UPDATE user SET email = {email} WHERE id = {id}"
            self.cursor.execute(query)
            self.connection.commit()
            return True
        except Exception as error:
            print(error)
            return False
    def update_user_password(self, id, password):
        try:
            query = f"UPDATE user SET password = {password} WHERE id = {id}"
            self.cursor.execute(query)
            self.connection.commit()
            return True
        except Exception as error:
            print(error)
            return False
    def delete_user(self, id):
        try:
            query = f"DELETE FROM user WHERE id = {id}"
            self.cursor.execute(query)
            self.connection.commit()
            return True
        except Exception as error:
            print(error)
            return False
    def get_user_information(self, id):
        try:
            query = f"SELECT * from user WHERE id = {id}"
            self.cursor.execute(query)
            result = self.cursor.fetchone()
            return result[1]
        except Exception as error:
            print(error)
            return False
    def get_user_id(self, name):
        try:
            query = f"SELECT id from user WHERE name = '{name}'"
            self.cursor.execute(query)
            result = self.cursor.fetchone()
            return result[0]
        except Exception as error:
            print(error)
            return False
    def login(self, email, password):
        try:
            query = f"SELECT COUNT(*) from user WHERE email = '{email}' AND password = '{password}'"
            self.cursor.execute(query)
            result = self.cursor.fetchone()
            return result
        except Exception as error:
            print(error)
            return False
    def new_key_relation(self, userId, publicKey):
        try:
            today = datetime.datetime.now()
            add_time = datetime.timedelta(days = 10)
            today = today + add_time
            query = f"INSERT INTO keyRelation (userId, publicKey, lifeTime) VALUES ('{userId}', '{publicKey}', '{today}')"
            self.cursor.execute(query)
            id = self.cursor.lastrowid
            self.connection.commit()
            return id
        except Exception as error:
            print(error)
            return False