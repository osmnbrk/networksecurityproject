import requests
import json

def main():
    url = "http://localhost:9797/publicKey"
    response = requests.get(
    url
    )
    with open("apiPublicKey.pem","wb") as myf:
        myf.write(response._content)
    
    print("Gelen Api Public Keyi")
    print(response._content)
    print("--------------------")
    r = rsaHelper.RSASecurity()

    url = "http://localhost:9797/register"
    payload = "Osman Berk,Asik,osmnbrktest@gmail.com,osmnbrk,Oba19734682"
    
    data = payload.encode("utf-8")
    enc_data = r.encrypt(data)
    with open("senddata.bin","wb") as myf:
        myf.write(enc_data)
    #print(json.dumps(payload))
    payload={}
    files=[
    ('data',('publicKey.pem',open('publicKey.pem','rb'),'application/octet-stream')),
    ('data',('senddata.bin',open('senddata.bin','rb'),'application/octet-stream'))
    ]
    headers = {}
    response = requests.request("PUT", url, headers=headers, data=payload, files=files)

    print("Kayıt olma işleminin cevabı")
    print(response._content)
    print("--------------------")


    url = "http://localhost:9797/login"
    payload = "osmnbrk,osmnbrktest@gmail.com,Oba19734682"
    
    data = payload.encode("utf-8")
    enc_data = r.encrypt(data)
    with open("senddata.bin","wb") as myf:
        myf.write(enc_data)
    #print(json.dumps(payload))
    payload={}
    files=[
    ('data',('senddata.bin',open('senddata.bin','rb'),'application/octet-stream'))
    ]
    headers = {}
    response = requests.request("POST", url, headers=headers, data=payload, files=files)
    
    print("Giriş yapma işleminin cevabı")
    print(response._content)
    print("--------------------")


    url = "http://localhost:9797/send"
    payload = "osmnbrktest@gmail.com,Oba19734682,osmnbrktest@gmail.com"
    
    data = payload.encode("utf-8")
    enc_data = r.encrypt(data)
    with open("senddata.bin","wb") as myf:
        myf.write(enc_data)
    payload = "test,Merhaba"
    
    data = payload.encode("utf-8")
    enc_data = r.encrypt(data)
    with open("mail.bin","wb") as myf:
        myf.write(enc_data)
    #print(json.dumps(payload))
    payload={}
    files=[
    ('data',('senddata.bin',open('senddata.bin','rb'),'application/octet-stream')),
    ('atc',('mail.bin',open('mail.bin','rb'),'application/octet-stream')),
    ('atc',('publicKey.pem',open('publicKey.pem','rb'),'application/octet-stream'))
    ]
    headers = {}
    response = requests.request("POST", url, headers=headers, data=payload, files=files)
    print("Mail gönderme işleminin cevabı")
    print(response._content)
    print("--------------------")

    payload={'mail': 'osmnbrktest@gmail.com',
    'password': 'Oba19734682'}
    files=[

    ]
    headers = {}

    response = requests.get(url, data = payload)

    zname = "data.zip"
    zfile = open(zname, 'wb')
    zfile.write(response.content)
    zfile.close()

    print("Mail görüntüleme işleminin cevabı")
    print(response.__dict__)
    print("--------------------")
main()