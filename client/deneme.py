from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP

"""def main():
    pubkey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC9lM5aypt3TBFw6ngHtpSqRxI3\nmfeXCnor+gq4FxHKaEhq1KviGjnrWnaMAIJLXm80V9FQkYRRGY8DAFZrH69pM/YH\nx1b24UZWXnw0X5bX3ntKDnVl4v7ujWJOL5dj764hkEkqvTXqwN3OMb+qutEzxCLU\nKLcIaCTuDocTArOdtQIDAQAB'
    msg = "test"
    keyDER = b64decode(pubkey)
    keyPub = RSA.importKey(keyDER)
    cipher = Cipher_PKCS1_v1_5.new(keyPub)
    cipher_text = cipher.encrypt(msg.encode())
    emsg = b64encode(cipher_text)
    print(keyPub.export_key())"""


def main(message, filename):
    with open (filename,"rb") as myf:
        api_public_key = RSA.import_key(myf.read())
    encryptor = PKCS1_OAEP.new(api_public_key)
    encrypted = encryptor.encrypt(message)
    return encrypted

def start():
    message = "Merhaba".encode("utf-8")
    filename = "apiPublicKey.pem"
    result = main(message,filename)
    filename = ""