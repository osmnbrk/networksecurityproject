from flask_restful import reqparse, abort, Resource, request
from endpoints import mailHelper, rsaHelper
import werkzeug, json
from werkzeug.datastructures import FileStorage

parser = reqparse.RequestParser()
parser.add_argument("data", type=werkzeug.datastructures.FileStorage, location='files')

class GetInbox(Resource):
    def get(self):
        r = rsaHelper.RSASecurity()
        files = request.files.to_dict(flat=False)
        for i, file in enumerate(files["data"]):
            name = file.filename
            if name == "data.json":
                file.save(f'{name}')
        with open ("data.json","r",encoding="utf-8") as myf:
            temp_dict = json.load(myf)
        data_dict = {}
        for key, value in temp_dict.items():
            data_dict[r.decryption(key, "apiKeys/privateKey.pem")] = r.decryption(value, "apiKeys/privateKey.pem")
        mh = mailHelper.MailHelper(data_dict["email"], data_dict["password"])
        inbox = mh.read_mail(data_dict["email"])
        return inbox
    def post(self):
        pass
    def put(self):
        pass
    def delete(self):
        pass