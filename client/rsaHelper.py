from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import base64
import os
class RSASecurity:
    def generate_key(self):
        private_key = RSA.generate(1024)
        public_key = private_key.publickey()
        os.remove("apiKeys/publicKey.pem")
        os.remove("apiKeys/privateKey.pem")
        with open("apiKeys/publicKey.pem","w") as myf:
            print("{}".format(public_key.exportKey().decode()), file=myf)
                
        with open("apiKeys/privateKey.pem","w") as myf:
            print("{}".format(private_key.exportKey().decode()), file=myf)

    def encrypt(self, message, filename):
        sample_string_bytes = message.encode("ascii")
        with open (filename,"rb") as myf:
            api_public_key = RSA.import_key(myf.read())
        encryptor = PKCS1_OAEP.new(api_public_key)
        encrypted = encryptor.encrypt(sample_string_bytes)
        base64_bytes = base64.b64encode(encrypted)
        base64_string = base64_bytes.decode("ascii")
        return base64_string
        
    def decryption(self, text, filename):
        base64_bytes = text.encode("ascii")
        sample_string_bytes = base64.b64decode(base64_bytes)
        with open (filename,"rb") as myf:
            private_key = RSA.import_key(myf.read())
        decryptor = PKCS1_OAEP.new(private_key)
        decrypted = decryptor.decrypt(sample_string_bytes)
        return decrypted.decode("ascii")