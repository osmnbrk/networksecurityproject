from flask_restful import reqparse, abort, Resource, request
from flask import send_from_directory
from endpoints import rsaHelper
parser = reqparse.RequestParser()

class ApiPublicKey(Resource):
    def get(self):
        r = rsaHelper.RSASecurity()
        return send_from_directory("apiKeys", r"publicKey.pem", as_attachment=True)
    def post(self):
        pass
    def put(self):
        pass
    def delete(self):
        pass