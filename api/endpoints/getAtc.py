from flask_restful import reqparse, abort, Resource, request
from flask import send_from_directory
from endpoints import rsaHelper
import werkzeug, json
from werkzeug.datastructures import FileStorage

parser = reqparse.RequestParser()
parser.add_argument("data", type=werkzeug.datastructures.FileStorage, location='files')

class GetAtc(Resource):
    def get(self):
        r = rsaHelper.RSASecurity()
        files = request.files.to_dict(flat=False)
        for i, file in enumerate(files["data"]):
            name = file.filename
            if name == "data.json":
                file.save(f'{name}')
        with open ("data.json","r",encoding="utf-8") as myf:
            temp_dict = json.load(myf)
        data_dict = {}
        for key, value in temp_dict.items():
            data_dict[r.decryption(key, "apiKeys/privateKey.pem")] = r.decryption(value, "apiKeys/privateKey.pem")
        return send_from_directory("client_files/" + data_dict["email"] + "/" + data_dict["id"], data_dict["filename"], as_attachment=True)
    def post(self):
        pass
    def put(self):
        pass
    def delete(self):
        pass