import smtplib,ssl,imaplib,email
import base64
import os
from email.header import decode_header
import webbrowser
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

class MailHelper:
    def __init__(self, mail, password):
        self.smtp_server_name = "smtp.gmail.com"
        self.context = ssl.create_default_context()
        self.mail = mail
        self.password = password
        
    def login(self):
        try:
            port = 465  # For SSL
            with smtplib.SMTP_SSL(self.smtp_server_name, 465, context=self.context) as server:
                login_flag = server.login(self.mail, self.password)
                if login_flag[1].decode("utf-8").split(" ")[1] == "Accepted":
                    return True
                else:
                    return False
        except Exception as err:
            print(err)
            return False
        
    def send_mail(self, receiver_email, subject, body, file_paths):
        try:
            message = MIMEMultipart()
            message["From"] = self.mail
            message["To"] = receiver_email
            message["Subject"] = subject
            message["Bcc"] = receiver_email
            message.attach(MIMEText(body, "plain"))
            print(file_paths)
            for i in range(len(file_paths)):
                with open(file_paths[i], "rb") as attachment:
                    part = MIMEBase("application", "octet-stream")
                    part.set_payload(attachment.read()) 
                encoders.encode_base64(part)
                part.add_header(
                    "Content-Disposition",
                    f"attachment; filename= {file_paths[i]}",
                )
                message.attach(part)

            text = message.as_string()
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL(self.smtp_server_name, 465, context=self.context) as server:
                login_flag = server.login(self.mail, self.password)
                if login_flag[1].decode("utf-8").split(" ")[1] == "Accepted":
                    server.sendmail(receiver_email, receiver_email, text)
                    return True
                else:
                    return False
                
        except Exception as err:
            print(err)
    def clean(self, text):
        return "".join(c if c.isalnum() else "_" for c in text)
    def read_mail(self, mail):
        imap = imaplib.IMAP4_SSL("imap.gmail.com")
        temp_list = []
        imap.login(self.mail, self.password)
        status, messages = imap.select("INBOX")
        messages = int(messages[0])
        for i in range(messages, 0, -1):

            res, msg = imap.fetch(str(i), "(RFC822)")
            for response in msg:
                if isinstance(response, tuple):
                    msg = email.message_from_bytes(response[1])
                    subject, encoding = decode_header(msg["Subject"])[0]
                    if isinstance(subject, bytes):
                        subject = subject.decode(encoding)
                    From, encoding = decode_header(msg.get("From"))[0]
                    if isinstance(From, bytes):
                        From = From.decode(encoding)
                    print("Subject:", subject)
                    print("From:", From)
                    if msg.is_multipart():
                        for part in msg.walk():
                            content_type = part.get_content_type()
                            content_disposition = str(part.get("Content-Disposition"))
                            try:
                                body = part.get_payload(decode=True).decode()
                            except:
                                pass
                            if content_type == "text/plain" and "attachment" not in content_disposition:
                                print(body)
                            elif "attachment" in content_disposition:
                                filename = part.get_filename()
                                if filename:
                                    file_list = []
                                    folder_name = "client_files/" + mail
                                    if not os.path.isdir(folder_name):
                                        os.mkdir(folder_name)
                                    folder_name = folder_name + "/" + str(i)
                                    if not os.path.isdir(folder_name):
                                        os.mkdir(folder_name)
                                    
                                    filepath = os.path.join(folder_name, filename)
                                    open(filepath, "wb").write(part.get_payload(decode=True))
                                    file_list.append(filename)
                    else:
                        content_type = msg.get_content_type()
                        body = msg.get_payload(decode=True).decode()
            temp_dict = {
                "ID" : i,
                "From" : From,
                "Subject" : subject,
                "Content" : body,
                "atc" : file_list
            }
            temp_list.append(temp_dict)
        imap.close()
        imap.logout()
        return temp_list