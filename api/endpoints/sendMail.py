from flask_restful import reqparse, abort, Resource, request
from endpoints import mailHelper, DBHelper, rsaHelper
import werkzeug, os, json
from werkzeug.datastructures import FileStorage

parser = reqparse.RequestParser()
parser.add_argument("data", type=werkzeug.datastructures.FileStorage, location='files')
parser.add_argument("atc", type=werkzeug.datastructures.FileStorage, location='files')

class SendMail(Resource):
    def get(self):
        pass
    def post(self):
        args = parser.parse_args()
        r = rsaHelper.RSASecurity()

        files = request.files.to_dict(flat=False)
        for i, file in enumerate(files["data"]):
            name = file.filename
            if name == "data.json":
                file.save(f'{name}')
            if name == "mail.json":
                file.save(f"data/{name}")
        with open ("data.json","r",encoding="utf-8") as myf:
            temp_dict = json.load(myf)
        data_dict = {}
        for key, value in temp_dict.items():
            data_dict[r.decryption(key, "apiKeys/privateKey.pem")] = r.decryption(value, "apiKeys/privateKey.pem")

        with open ("data/mail.json","r",encoding="utf-8") as myf:
            temp_dict = json.load(myf)
        mail_dict = {}
        for key, value in temp_dict.items():
            mail_dict[r.decryption(key, "apiKeys/privateKey.pem")] = value

        file_paths = []
        file_path = None
        files = request.files.to_dict(flat=False)
        
        if "atc" in files:
            for i, file in enumerate(files["atc"]):
                file_path = file.filename
            print(file_path)
            file.save(file_path)
            file_paths.append(file_path)
        if file_path == None:
            file_path == ""
        mh = mailHelper.MailHelper(data_dict["sender"], data_dict["password"])
        result = mh.send_mail(data_dict["reciever"], mail_dict["title"], mail_dict["content"], file_paths)
        os.remove("data.json")
        if result == True:
            return True
        else:
            return False
    def put(self):
        pass
    def delete(self):
        pass