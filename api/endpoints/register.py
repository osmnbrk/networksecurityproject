from flask_restful import reqparse, abort, Resource, request
from endpoints import mailHelper, DBHelper, rsaHelper
import werkzeug, os, json
from werkzeug.datastructures import FileStorage

parser = reqparse.RequestParser()
parser.add_argument("data", type=werkzeug.datastructures.FileStorage, location='files')

class Register(Resource):
    def get(self):
        pass
    def post(self):
        pass
    
    def put(self):
        args = parser.parse_args()
        r = rsaHelper.RSASecurity()
        db = DBHelper.DBHelper()
        files = request.files.to_dict(flat=False)
        for i, file in enumerate(files["data"]):
            name = file.filename
            if name == "data.json":
                file.save(f'{name}')
            if name == "publicKey.pem":
                file.save(f"clientPubKey/{name}")
        with open ("data.json","r",encoding="utf-8") as myf:
            temp_dict = json.load(myf)
        data_dict = {}
        for key, value in temp_dict.items():
            data_dict[r.decryption(key, "apiKeys/privateKey.pem")] = r.decryption(value, "apiKeys/privateKey.pem")
        result = db.new_user(data_dict["name"],data_dict["surname"],data_dict["email"],data_dict["password"])
        db.new_key_relation(result, data_dict["email"] + ".pem")
        mail = data_dict["email"]
        if os.path.exists(f"clientPubKey/{mail}.pem") == False:
            os.rename("clientPubKey/publicKey.pem" , f"clientPubKey/{mail}.pem")
        else:
            os.remove(f"clientPubKey/{mail}.pem")
            os.rename("clientPubKey/publicKey.pem" , f"clientPubKey/{mail}.pem")
        os.remove("data.json")
        return result
    def delete(self):
        pass