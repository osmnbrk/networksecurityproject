import rsaHelper
import requests, json

def create_req(path, type, payload, files):
    url = "http://localhost:9797/" + path

    if type == "GET":
        response = requests.get(url)
    elif type == "POST":
        response = requests.request("POST", url, headers={}, data=payload, files=files)
    elif type == "PUT":
        response = requests.request("PUT", url, headers={}, data=payload, files=files)
    return response


def get_api_pub_key():
    path = "publicKey"
    type = "GET"
    payload = {}
    files = []
    response = create_req(path, type, payload, files)
    with open("apiPublicKey.pem","wb") as myf:
        myf.write(response._content)
    

def get_pub():
    client = "osmnbrk"
    path = "/clientPub"
    type = "GET"
    payload = {"mail" : "osmnbrktest@gmail.com"}
    files = []
    response = create_req(path, type, payload, files)
    with open("reciever.pem","wb") as myf:
        myf.write(response._content)

def register():
    path = "register"
    type = "PUT"
    payload = {}
    

    r = rsaHelper.RSASecurity()
    mydict = {
        r.encrypt("name", "apiPublicKey.pem") : r.encrypt("Osman Berk", "apiPublicKey.pem"),
        r.encrypt("surname", "apiPublicKey.pem") : r.encrypt("Asik", "apiPublicKey.pem"),
        r.encrypt("email", "apiPublicKey.pem") : r.encrypt("osmnbrktest@gmail.com", "apiPublicKey.pem"),
        r.encrypt("password", "apiPublicKey.pem") : r.encrypt("Oba19734682", "apiPublicKey.pem")
    }
    with open("data.json","w",encoding='utf-8') as myf:
        json.dump(mydict,myf,ensure_ascii=False)

    files=[
    ('data',('publicKey.pem',open('publicKey.pem','rb'),'application/octet-stream')),
    ('data',('data.json',open('data.json','rb'),'application/octet-stream'))
    ]
    response = create_req(path, type, payload, files)
    print(response.__dict__)

def login():
    path = "login"
    type = "POST"
    payload = {}
    

    r = rsaHelper.RSASecurity()
    mydict = {
        r.encrypt("email", "apiPublicKey.pem") : r.encrypt("osmnbrktest@gmail.com", "apiPublicKey.pem"),
        r.encrypt("password", "apiPublicKey.pem") : r.encrypt("Oba19734682", "apiPublicKey.pem")
    }
    with open("data.json","w",encoding='utf-8') as myf:
        json.dump(mydict,myf,ensure_ascii=False)

    files=[
    ('data',('data.json',open('data.json','rb'),'application/octet-stream'))
    ]
    response = create_req(path, type, payload, files)
    print(response.__dict__)

def send_mail():
    path = "send"
    type = "POST"
    payload = {}
    

    r = rsaHelper.RSASecurity()
    mydict = {
        r.encrypt("sender", "apiPublicKey.pem") : r.encrypt("osmnbrktest@gmail.com", "apiPublicKey.pem"),
        r.encrypt("reciever", "apiPublicKey.pem") : r.encrypt("osmnbrktest@gmail.com", "apiPublicKey.pem"),
        r.encrypt("password", "apiPublicKey.pem") : r.encrypt("Oba19734682", "apiPublicKey.pem")
    }
    with open("data.json","w",encoding='utf-8') as myf:
        json.dump(mydict,myf,ensure_ascii=False)

    mydict = {
        r.encrypt("title", "apiPublicKey.pem") : r.encrypt("Baslik", "reciever.pem"),
        r.encrypt("content", "apiPublicKey.pem") : r.encrypt("Icerik", "reciever.pem")
    }
    with open("mail.json","w",encoding='utf-8') as myf:
        json.dump(mydict,myf,ensure_ascii=False)

    files=[
    ('data',('data.json',open('data.json','rb'),'application/octet-stream')),
    ('data',('mail.json',open('mail.json','rb'),'application/octet-stream')),
    ('atc',('mail.json',open('mail.json','rb'),'application/octet-stream')),
    ('atc',('test.pdf',open('test.pdf','rb'),'application/octet-stream'))

    ]
    response = create_req(path, type, payload, files)
    print(response.__dict__)


def main():
    get_api_pub_key()
    register()
    login()
    get_pub()
    send_mail()

main()